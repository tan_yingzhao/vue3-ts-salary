import { MockMethod } from 'vite-plugin-mock'
import {mockData} from '../src/api/deptSalaData.api' 


let deptList = {
  deptName:'市分公司',
  level:1,
  subDepts:[ {deptName:'长安',level:2} ] };


  interface ApiType {
    
  }
  


export default [
  {
    url: '/api/createUser',
    method: 'post',
    response: ({ body, query }) => {
      console.log('body>>>>>>>>', body)
      console.log('query>>>>>>>>', query)
      return {
        code: 0,
        message: 'ok',
        data: true,
      }
    },
  },

  { //根据传入的参数，deptId，返回对应的分局数据
    url: '/api/dept',
    method: 'get',
    response: ({ body, query }) => {
      console.log('body>>>>>>>>', body)
      console.log('query>>>>>>>>', query)
      return {
        code: 0,
        message: 'ok',
        data: {deptList:deptList},
      }
    },
  },

  {
      url:'/api/mockData',
      method:'get',
      response:({body,query}) =>{
        console.log('query>>>>>>>>', query)
        console.log('mockData');
        return {
          code: 0,
          message: 'ok',
          data: mockData(query.bRand) ? mockData(true):undefined,
        } 
      }
  } 

] as MockMethod[];
