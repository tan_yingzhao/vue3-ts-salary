import { defineConfig, UserConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import WindiCSS from "vite-plugin-windicss";
import { configMockPlugin } from "./src/mockPlugin";

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }): UserConfig => {
  // console.log("command, mode ", command, mode);
  const bProd = mode === 'production';
  return {
    plugins: [vue(), WindiCSS(), configMockPlugin(bProd)],
  };
});
