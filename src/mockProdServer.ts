import { createProdMockServer } from 'vite-plugin-mock/es/createProdMockServer'


import  userApi from '../mock/user'
export const  mockModules = [...userApi];

export function setupProdMockServer() {
  createProdMockServer(mockModules)
}
