import  myMarker from './myMarker'



const typeArr = [ 'flash','flash','bounce']
 
const randomMakerArr = (mapInstance,len,ptX,ptY,dataArr) => {
  const arr = Array(len)
    .fill(0)
    .map((_, i) => {
        const item = dataArr?.length > i ? dataArr[i]:{};
        let {lat,lng,stateId,clientName,lstTime,bDeal} = item;
        // console.log(i,lat,lng,stateId,clientName,lstTime,bDeal)
        let x = ptX +lat  ?? ptX + (Math.ceil(Math.random() * 200) - 100) / 2000;
        let y = ptY +lng  ?? ptY + (Math.ceil(Math.random() * 200) - 100) / 2000;
        stateId = stateId ?? Math.floor(Math.random() * 3);
      return new myMarker({
        map:mapInstance,  
        position: new TMap.LatLng(x, y),
        type:  typeArr[stateId],
        stateId,
        clientName: clientName ?? `客户${i}`,
        lstTime: lstTime ?? new Date().toLocaleString(),
        bDeal,
      }).on('click',infoDialog);
    });

  return arr;
};
const x = 23.020853, y = 113.751861;
const infoDialog = (event)=>{
        const {clientName,lstTime,stateText,bDeal,stateId} = {...event};
        let info = stateId === 2 ? (bDeal === 0 ? '未处理' :'已处理'):'';
        
        if(infoWin) {
            infoWin.setContent(
            `<div
            style="text-align: center;
                white-space: nowrap;
                margin: 0px;
                font-size: small;
                color: rgb(51 38 101 / 83%);
                font-weight: 600;
                width: auto;
                height: auto;">
                <p>${clientName} ${stateText} ${info}</p>
                <p>${lstTime}</p>
            </div>`
	   );
	   infoWin.setPosition(event.latLng)
       infoWin.open()
	    }
    }
let map = null,markerArr = [] , bounds = null ,infoWin = null;


export default  function initMap(dom,dataArr) {
    console.log('TMap',TMap,dom)
    
    // 初始化地图
    // if(!map) {
    if(map) {
        map.destroy();
        map = null 
        bounds=null
        infoWin=null
        markerArr=null
        
    }


    map = new TMap.Map(dom, {
        zoom: 12, // 设置地图缩放级别
        center: new TMap.LatLng(x + 0.002, y+0.015) // 设置地图中心点坐标
    });
       
    //提示窗体
    infoWin = new TMap.InfoWindow({
        map: map,
        position:map.getCenter(),
        enableCustom:true,
        zIndex:999,
    });
    infoWin.close();    
      
    
    if(dataArr && Array.isArray(dataArr) && dataArr.length > 0) {
         
        markerArr =randomMakerArr(map,dataArr.length,x,y,dataArr);
        console.log('更新标注点',markerArr)
 
    }else {
       markerArr = randomMakerArr(map,20,x,y);
       console.log('产生随机标注点',markerArr)
    }
    
    // return;
    // console.log('markerArr',markerArr)
    // return;   
    
      // 初始化，边界
   bounds = new TMap.LatLngBounds();

  // 设置自适应显示marker
  (function showMarker() {
    // 判断标注点是否在范围内
    markerArr.forEach(function (item) {
      // 若坐标点不在范围内，扩大bounds范围
      if (bounds.isEmpty() || !bounds.contains(item.position)) {
        bounds.extend(item.position);
      }
    });
    // 设置地图可视范围

    map.fitBounds(bounds, {
      padding: 100 // 自适应边距
    });
  })();

}
