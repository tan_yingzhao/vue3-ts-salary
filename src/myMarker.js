export default function myMarker(options) {
    //let this.mydom;
    this.mydom = null;
    TMap.DOMOverlay.call(this, options);
    // console.log('myMarker.options',options)
}
myMarker.prototype = new TMap.DOMOverlay();




// 初始化
myMarker.prototype.onInit = function (options) {
    this.position = options.position;
    this.type = options.type; // 当前marker的类型，是跳动或飞入
    this.stateId = options.stateId;
    this.clientName = options.clientName;
    this.lstTime    = options.lstTime;
    this.stateText  = stateArr[this.stateId].text;
    this.zIndex     = this.stateId;
    this.bDeal      = options.bDeal;
}


//自定义DOM覆盖物 - 继承DOMOverlay
const stateArr = [
    {
        id:0,
        text:'正常',
        url:  'https://s1.aigei.com/src/img/png/f0/f0e51a24376e443c9d6fbc991af8d7b9.png?|watermark/3/image/aHR0cHM6Ly9zMS5haWdlaS5jb20vd2F0ZXJtYXJrLzUwMC0wLnBuZz9lPTE3MzU0ODgwMDAmdG9rZW49UDdTMlhwemZ6MTF2QWtBU0xUa2ZITjdGdy1vT1pCZWNxZUpheHlwTDpwT2FUbDdqSURXdXFDMFRlZTd0LVUtbUFVZjQ9/dissolve/40/gravity/NorthWest/dx/-13/dy/9/ws/0.0/wst/0&e=1735488000&token=P7S2Xpzfz11vAkASLTkfHN7Fw-oOZBecqeJaxypL:6ZZMRB-B_Lcrt2A16ATv2V_TS-o=',
    },
    {
        id:1,
        text:'离线',
        url: 'https://s1.chu0.com/src/img/png/29/29489ca4c01d4bafb0affe90cf71b81f.png?imageMogr2/auto-orient/thumbnail/!234x234r/gravity/Center/crop/234x234/quality/85/&e=1735488000&token=1srnZGLKZ0Aqlz6dk7yF4SkiYf4eP-YrEOdM1sob:TRkjn-vh1g8t4FOWw0-f6V3HEhg=',
    },
    {
        id:2,
        text:'告警',
        url:'https://s1.chu0.com/src/img/png/19/19092cba04b9459cba38ab5cde3db5f5.png?e=1735488000&token=1srnZGLKZ0Aqlz6dk7yF4SkiYf4eP-YrEOdM1sob:xN7y9YlvdFvnipzbZpLOAQZ3-Dg=',
    },    
]
// 创建
myMarker.prototype.createDOM = function () {
    this.mydom = document.createElement('img');  // 新建一个img的dom
    this.mydom.src = stateArr[this.stateId].url;
    this.mydom.style.cssText = [
        'position: absolute;',
        'top:  0px;',
        'left: 0px;',
        'width:32px;',
        // 'height:32px;',
    ].join('');
    switch (this.type) {
        case 'bounce':
            if(this.bDeal === 0) {
            //    this.mydom.setAttribute('class', 'markerBounce');  // 给新建的dom添加marker类，添加跳动效果
               this.mydom.setAttribute('class', 'markerShake');  // 给新建的dom添加marker类，添加跳动效果
            }
            
            break;
        case 'flash':
            this.mydom.setAttribute('class', 'markerFlash');		// 给新建的dom添加marker类，添加飞入效果
            break;
            
    }

    // click事件监听
    this.onClick = () => {
        // DOMOverlay继承自EventEmitter，可以使用emit触发事件
        this.emit('click',{
            latLng:this.position,
            clientName:this.clientName,
            lstTime:this.lstTime,
            stateText:this.stateText,
            bDeal:this.bDeal,
            stateId:this.stateId,
        });
    };

    this.mydom.addEventListener('click', this.onClick)
    return this.mydom;
}

// 更新DOM元素，在地图移动/缩放后执行
myMarker.prototype.updateDOM = function () {
    if (!this.map) {
        return;
    }
    let pixel = this.map.projectToContainer(this.position); // 经纬度坐标转容器像素坐标
    let left = pixel.getX() - this.dom.clientWidth / 2 + 'px';
    let top = pixel.getY() + 'px';
    // 使用top/left将DOM元素定位到指定位置
    this.dom.style.top = top;
    this.dom.style.left = left;
}


// 销毁时需解绑事件监听
myMarker.prototype.onDestroy = function() {
    if (this.onClick) {
        this.dom.removeEventListener(this.onClick);
    }
};



