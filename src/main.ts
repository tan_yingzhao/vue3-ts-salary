import { createApp } from 'vue'
import App from './App.vue'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

// import naive from 'naive-ui'
// 通用字体
// import 'vfonts/Lato.css'
// 等宽字体
// import 'vfonts/FiraCode.css'
import 'virtual:windi.css'

import {router} from './routers'



const app = createApp(App);

(app.config as any).productionTip  = false;

// 注册全局指令
app.directive('setFocus',{
    mounted(el,binding) {
        if(binding.value)
        //由于a-input-number，外部嵌套多层div，导致无法直接设置输入焦点
        //通过获取输入的input标签，再设置输入焦点即可
            if (el.getElementsByTagName("input")  && el.getElementsByTagName("input").length > 0)
            el.getElementsByTagName("input")[0].focus();
    } 
})


app.use(Antd).use(router).mount('#app');

//尝试写文档，分析整个流程
//待解决的问题：在npm run serve——onMounted触发的mock API无效
//打包文件过大，取消全部导入，换成部分导入（按需导入？）
//解决后，挂载到服务器上
//之后了解Nuxt.js前后端框架

//对API加入类型限制


