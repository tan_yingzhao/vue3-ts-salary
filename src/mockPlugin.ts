import { viteMockServe } from 'vite-plugin-mock'

// import { UserConfigExport, ConfigEnv } from 'vite'
 



export function configMockPlugin(bProdMock: boolean) {


  return viteMockServe({
    ignore: /^\_/,
    mockPath: 'mock',
    //本地mock
    localEnabled: !bProdMock,
    prodEnabled: bProdMock,
    //打包时，不让mock打包到最终代码上
    injectCode: `
      import { setupProdMockServer } from './mockProdServer';
      setupProdMockServer();
      `,
  });
}
