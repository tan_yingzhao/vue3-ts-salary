import {IMonitorInfo,IMonitorStateInfo,MarkerCssClass,ITreeData} from './monitorData.d'
import dayjs from "dayjs";

  const defStateInfoArr: IMonitorStateInfo[] = [
    {
      stateId: 0,
      stateText: "正常",
      imgUrl:'https://s1.aigei.com/src/img/png/f0/f0e51a24376e443c9d6fbc991af8d7b9.png?|watermark/3/image/aHR0cHM6Ly9zMS5haWdlaS5jb20vd2F0ZXJtYXJrLzUwMC0wLnBuZz9lPTE3MzU0ODgwMDAmdG9rZW49UDdTMlhwemZ6MTF2QWtBU0xUa2ZITjdGdy1vT1pCZWNxZUpheHlwTDpwT2FUbDdqSURXdXFDMFRlZTd0LVUtbUFVZjQ9/dissolve/40/gravity/NorthWest/dx/-13/dy/9/ws/0.0/wst/0&e=1735488000&token=P7S2Xpzfz11vAkASLTkfHN7Fw-oOZBecqeJaxypL:6ZZMRB-B_Lcrt2A16ATv2V_TS-o=',
      cssClass: MarkerCssClass.normal,
    },
    {
      stateId: 1,
      stateText: "离线",
      imgUrl:
        "https://s1.chu0.com/src/img/png/29/29489ca4c01d4bafb0affe90cf71b81f.png?imageMogr2/auto-orient/thumbnail/!234x234r/gravity/Center/crop/234x234/quality/85/&e=1735488000&token=1srnZGLKZ0Aqlz6dk7yF4SkiYf4eP-YrEOdM1sob:TRkjn-vh1g8t4FOWw0-f6V3HEhg=",
      cssClass: MarkerCssClass.normal,
    },
    {
      stateId: 2,
      stateText: "告警",
      imgUrl:
        "https://s1.chu0.com/src/img/png/19/19092cba04b9459cba38ab5cde3db5f5.png?e=1735488000&token=1srnZGLKZ0Aqlz6dk7yF4SkiYf4eP-YrEOdM1sob:xN7y9YlvdFvnipzbZpLOAQZ3-Dg=",
      cssClass: MarkerCssClass.warning,
    },
  ];
 
  const defTreeData: ITreeData[]= [{
    title: '东莞市',
    key: '0',
    children: [{
    title: '万江街道',
    key: '0-0',
      children: [{
          title: '新村',
          key: '0-0-0',
        }, {
          title: '莫屋',
          key: '0-0-1',
        }, {
          title: '水蛇涌',
          key: '0-0-2',
        }],
    }, {
    title: '南城街道',
    key: '0-1',
      children: [{
        title: '宏远工业区',
        key: '0-1-0',
      }],
    }, {
    title: '东城街道',
    key: '0-2',
      children: [{
        title: '旗峰公园',
        key: '0-2-0',
      }, {
        title: '万达广场',
        key: '0-2-1',
      }],
    }],
  }];

function getRandomMarkerData(size:number,centerPt:number[],keys:string[]=[]):IMonitorInfo[] {
    
    return Array(size)
      .fill(0)
      .map((_, i) => {
        const [lng, lat] = centerPt;
        const lngTmp = lng + (Math.ceil(Math.random() * 200) - 100) / 2000;
        const latTmp = lat + (Math.ceil(Math.random() * 200) - 100) / 2000;
        const stateId = Math.floor(Math.random() * 3);
        const bDeal = stateId === 2 ? 0 : -1;
        const key = keys && keys.length > 0 ?  keys[   Math.floor( Math.random() * keys.length )  ] :undefined;
        return {
          key:key,
          lat: latTmp,
          lng: lngTmp,
          monitorName: `监控${i}`,
          monitorId: `${i}`,
          lstTime: dayjs(Date()).format("YYYY-MM-DD HH:mm:ss"),
          stateId,
          clientName: `客户${i}`,
          bDeal,
        } as IMonitorInfo;
      });

}


const getKeys =(datas:ITreeData[],res:string[]=[])=>{
  
  if( Array.isArray(datas) ){
      datas.forEach(i => {
          res.push(i.key)
          if(i.children) {
             getKeys(i.children,res)
          }  
      })
  }
  return res 
  
} 


const getChildKeys = (datas:ITreeData[],res:string[]=[])=>{
  
  if( Array.isArray(datas) ){
      datas.forEach(i => {
          
          if(i.children) {
             getChildKeys(i.children,res) 
          }else {
             res.push(i.key)   
          }
          
      })
  }
  return res 
  
} 

  export {  defStateInfoArr ,defTreeData,getRandomMarkerData , getKeys,getChildKeys}