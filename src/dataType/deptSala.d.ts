

//分局成本，超额
interface IDeptMoney {
    A_gz_cost: number;
    A_fgz_cost: number;
    A_ldjs_cost: number;
    BC_js_cost: number;
    BC_ldjs_cost: number;
    level:number;
    desc?:string;
    children?:IDeptMoney[];
  }
  
  //分局发放使用成本
  interface IDeptSalaUse {
    salaType: string;
    useCost: number;
  }
  

import {salaState} from '../api/deptSalaData.api';
  // 分局发放数据明细
  interface IDeptSala {
    belong: string;
    itemName:string;
    empType: string;
    salaCost: number;
    orgSalaType: string;
    state?:salaState;
    deptUses?: IDeptSalaUse[];
    remainCost?:number;
    
  }
  
  interface IDept_mon_sala {
    dept: { deptName: string };
    yearMon: number;
    salaItem:string;
    remain_costs?: IDeptMoney;
    over_costs?: IDeptMoney;
    detail_costs?: IDeptSala[];
  }
 

  export {IDeptMoney,IDeptSalaUse,IDeptSala,IDept_mon_sala}