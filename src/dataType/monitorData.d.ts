//const typeArr = [ 'flash','flash','bounce']
/*定义CSS class动画*/ 
export enum  MarkerCssClass {
    normal = 'markerFlash',
    // warning= 'markerShake',
    warning = 'markerBounce',
}

interface IMonitorInfo {
    key?:string;
    lat:number;
    lng:number;
    monitorName:string;
    monitorId:string;
    lstTime:string;
    stateId:number;
    clientName:string;
    bDeal:number;
}


interface IMonitorStateInfo {
    stateId:number;
    stateText:string;
    imgUrl:string;
    cssClass:MarkerCssClass;
}
 
interface ITreeData {
    key:string;
    title:string;
    children?:ITreeData[];
}

export {IMonitorInfo,IMonitorStateInfo,ITreeData}