import { ref, unref, computed, Ref, ComputedRef} from "vue";
import {
  IDeptMoney,
  IDeptSala,
  IDept_mon_sala,
} from "../dataType/deptSala";
import axios from "axios";
import mockjs from "mockjs";


//发放状态
export enum salaState {
  notOk = -1,
  isOk = 0,
  isChecked = 1,
}


function NumRound(value:number,nxtCt:number):number{
   const numStr = value.toFixed(nxtCt);
   return Number.parseFloat(numStr);
}

function getDeptList() {
  axios
    .get("/api/dept", {
      params: {
        deptId: 10201,
      },
    })
    .then((res) => {
      console.log("res:::", res);

    })
    .catch((err) => {
      console.log("err:::", err);
    });
}

function getMockData(bRand: boolean): Promise<IDept_mon_sala | undefined> {
  let tmp: IDept_mon_sala | undefined;
  return new Promise((resolve, reject) => {
    axios
      .get("/api/mockData", {
        params: {
          bRand,
        },
      })
      .then((res) => {
        tmp = res.data.data;
        // console.log("getMockData:::成功",tmp);
        resolve(tmp);
      })
      .catch((err) => {
        tmp = undefined;
        console.log("err:::", err);
        reject("获取mockData失败");
      });
  });
}

function mockData(bRand: boolean): IDept_mon_sala {

  const tmp1: IDept_mon_sala = {
    dept: { deptName: "信息局" },
    yearMon: 202111,
    salaItem: "按量计酬",
    //当月结余
    remain_costs: {
      A_gz_cost: 100000,
      A_fgz_cost: 100000,
      A_ldjs_cost: 100000,
      BC_js_cost: 100000,
      BC_ldjs_cost: 100000,
      level: 20000,
    },
    //当月超额
    over_costs: {
      A_gz_cost: 50000,
      A_fgz_cost: 50000,
      A_ldjs_cost: 50000,
      BC_js_cost: 50000,
      BC_ldjs_cost: 50000,
      level: 30000,
    },
    //当月发放
    detail_costs: [
      {
        itemName: "按量记仇",
        belong: "A",
        empType: "职聘工",
        salaCost: 120000,
        orgSalaType: "工资总额",
      },
      {
        itemName: "按量记仇",
        belong: "A",
        empType: "劳务工",
        salaCost: 60000,
        orgSalaType: "非工资总额",
      },
      {
        itemName: "按量记仇",
        belong: "A",
        empType: "劳务承揽",
        salaCost: 60000,
        orgSalaType: "非工资总额",
      },
      {
        itemName: "按量记仇",
        belong: "A",
        empType: "外包人员",
        salaCost: 60000,
        orgSalaType: "非工资总额",
      },

      {
        itemName: "按量记仇",
        belong: "B",
        empType: "职聘工",
        salaCost: 50000,
        orgSalaType: "基数",
      },
      {
        itemName: "按量记仇",
        belong: "B",
        empType: "劳务承揽",
        salaCost: 50000,
        orgSalaType: "基数",
      },
      {
        itemName: "按量记仇",
        belong: "C",
        empType: "劳务承揽",
        salaCost: 50000,
        orgSalaType: "基数",
      },
      {
        itemName: "按量记仇",
        belong: "C",
        empType: "外包人员",
        salaCost: 50000,
        orgSalaType: "基数",
      },
    ],
  };

  const deptNameArr: string[] = [
    "AAA分公司",
    "BBB分公司",
    "CCC分公司",
    "DDD分公司",
    "EEE分公司",
    "FFF分公司",
    "GGG分公司",
    "HHH分公司",
    "III分公司",
    "JJJ分公司",
    "KKK分公司",
  ];

  
  const tmp = {
    dept: { deptName: mockjs.mock({ "deptName|1": deptNameArr }).deptName },
    yearMon: 202100 + mockjs.mock({ "number|1-12": 1 }).number,
    salaItem: "按量计酬",
    //当月结余
    remain_costs: {
      A_gz_cost: mockjs.mock({ "number|1-200000.1-2": 1 }).number,
      A_fgz_cost: mockjs.mock({ "number|1-300000.1-2": 1 }).number,
      A_ldjs_cost: mockjs.mock({ "number|1-300000.1-2": 1 }).number,
      BC_js_cost: mockjs.mock({ "number|1-500000.1-2": 1 }).number,
      BC_ldjs_cost: mockjs.mock({ "number|1-300000.1-2": 1 }).number,
      level: 2,
    },
    //当月超额
    over_costs: {
      A_gz_cost: mockjs.mock({ "number|1-100000": 1 }).number,
      A_fgz_cost: mockjs.mock({ "number|1-100000": 1 }).number,
      A_ldjs_cost: mockjs.mock({ "number|1-100000": 1 }).number,
      BC_js_cost: mockjs.mock({ "number|1-100000": 1 }).number,
      BC_ldjs_cost: mockjs.mock({ "number|1-100000": 1 }).number,
      level: 3,
    },
    //当月发放
    detail_costs: [
      {
        itemName: "按量记仇",
        belong: "A",
        empType: "职聘工",
        salaCost: mockjs.mock({ "number|1-190000.1-2": 1 }).number,
        orgSalaType: "工资总额",
      },
      {
        itemName: "按量记仇",
        belong: "A",
        empType: "劳务工",
        salaCost: mockjs.mock({ "number|1-190000.1-2": 1 }).number,
        orgSalaType: "非工资总额",
      },
      {
        itemName: "按量记仇",
        belong: "A",
        empType: "劳务承揽",
        salaCost: mockjs.mock({ "number|1-190000.1-2": 1 }).number,
        orgSalaType: "非工资总额",
      },
      {
        itemName: "按量记仇",
        belong: "A",
        empType: "外包人员",
        salaCost: mockjs.mock({ "number|1-190000.1-2": 1 }).number,
        orgSalaType: "非工资总额",
      },

      {
        itemName: "按量记仇",
        belong: "B",
        empType: "职聘工",
        salaCost: mockjs.mock({ "number|1-190000.1-2": 1 }).number,
        orgSalaType: "基数",
      },
      {
        itemName: "按量记仇",
        belong: "B",
        empType: "劳务承揽",
        salaCost: mockjs.mock({ "number|1-190000.1-2": 1 }).number,
        orgSalaType: "基数",
      },
      {
        itemName: "按量记仇",
        belong: "C",
        empType: "劳务承揽",
        salaCost: mockjs.mock({ "number|1-190000.1-2": 1 }).number,
        orgSalaType: "基数",
      },
      {
        itemName: "按量记仇",
        belong: "C",
        empType: "外包人员",
        salaCost: mockjs.mock({ "number|1-190000.1-2": 1 }).number,
        orgSalaType: "基数",
      },
    ],
  } as IDept_mon_sala;

  return bRand ? tmp : tmp1;
}

//返回分局当月发放明细 
function getDeptDetailTabInfo(detail_costs: Ref<IDeptSala[]>): any {
  //分局发放明细，table所需属性
  const detail_cols = [
    { title: "归属", key: "belong", dataIndex: "belong" },
    { title: "工种", key: "empType", dataIndex: "empType" },
    {
      title: "发放金额(元)",
      key: "salaCost",
      dataIndex: "salaCost",
      align: "right",
    },
    { title: "默认成本", key: "orgSalaType", dataIndex: "orgSalaType" },
    { title: "可分配", key: "deptUses", dataIndex: "deptUses" },
    {
      title: "待分配差额",
      key: "remainCost",
      dataIndex: "remainCost",
      align: "right",
    },
    { title: "结果", key: "state", dataIndex: "state" },
  ];

  if (detail_costs)
    detail_costs.value?.forEach((item: IDeptSala) => {
      item.deptUses = [];
      item.state = salaState.notOk;
      item.remainCost = item.salaCost;
      item.deptUses.push({
        salaType: item.orgSalaType,
        useCost: 0,
      });
      if (item.orgSalaType !== "劳动竞赛") {
        item.deptUses.push({
          salaType: "劳动竞赛",
          useCost: 0,
        });
      }
    });

  function rowKey(record: any): string {
    return record.belong + "," + record.itemName + "," + record.empType;
  }

  return { detail_costs, detail_cols, rowKey };
}

//返回分局结余，超额数据的数据表信息，数据，列，rowkey
//加入当前的发放明细，返回当前发放统计，结余数据
function getDeptSalaTableInfo(
  remain_costs: Ref<IDeptMoney> ,
  over_costs: Ref<IDeptMoney>,
  detail_costs: Ref<IDeptSala[]>
): any {
  // 1.结余+超额，data
  //需要将发放明细中的分配进行提取，之后形成统计
  

  const  use_costs:ComputedRef< IDeptMoney[] > = computed(()=>{
 

    if(remain_costs.value && over_costs.value &&  detail_costs.value){
      let use_cost = {
        A_gz_cost:  NumRound( (remain_costs.value.A_gz_cost ?? 0) + (over_costs.value.A_gz_cost ?? 0) , 2 ) ,
        A_fgz_cost: NumRound( (remain_costs.value.A_fgz_cost ?? 0) + (over_costs.value.A_fgz_cost ?? 0) , 2 ) ,
        A_ldjs_cost:NumRound( (remain_costs.value.A_ldjs_cost ?? 0) + (over_costs.value.A_fgz_cost ?? 0), 2 ) ,
        BC_js_cost: NumRound( (remain_costs.value.A_fgz_cost ?? 0) + (over_costs.value.A_fgz_cost ?? 0), 2 ) ,
        BC_ldjs_cost: NumRound( (remain_costs.value.A_fgz_cost ?? 0) + (over_costs.value.A_fgz_cost ?? 0) , 2 ) ,
  
        level: 1,
        desc: "结余+超额",
        children: [
          { ...unref(remain_costs) , desc: "结余" },
          { ...unref(over_costs), desc: "超额" },
        ],
      };
  
  
      //let use_costs:ComputedRef< IDeptMoney[] >;    
      //需要将发放明细中的分配进行提取，之后形成统计
        
        let tmp_cost = {
            A_gz_cost: 0,
            A_fgz_cost: 0,
            A_ldjs_cost: 0,
            BC_js_cost: 0,
            BC_ldjs_cost: 0,
            level: 2,
            desc: "分配合计",
          },
          remain_cost = {
            A_gz_cost: use_cost.A_gz_cost,
            A_fgz_cost: use_cost.A_fgz_cost,
            A_ldjs_cost: use_cost.A_ldjs_cost,
            BC_js_cost: use_cost.BC_js_cost,
            BC_ldjs_cost: use_cost.BC_ldjs_cost,
            level: 3,
            desc: "分配结余",
          };
  
        detail_costs.value?.flatMap((item) => {
            //将分配的使用成本数组，二维转一维
            return item.deptUses?.map((i) => {
              return { ...i, belong: item.belong };
            });
          })
          .forEach((cost) => {
            if ((cost?.useCost as number) > 0) {
  
              if (cost?.belong === "A" && cost?.salaType === "工资总额") {
                tmp_cost.A_gz_cost += cost.useCost;
                remain_cost.A_gz_cost -= cost.useCost;
              } else if (
                cost?.belong === "A" &&
                cost?.salaType === "非工资总额"
              ) {
                tmp_cost.A_fgz_cost += cost.useCost;
                remain_cost.A_fgz_cost -= cost.useCost;
              } else if (cost?.belong === "A" && cost?.salaType === "劳动竞赛") {
                tmp_cost.A_ldjs_cost += cost.useCost;
                remain_cost.A_ldjs_cost -= cost.useCost;
              } else if (
                (cost?.belong === "B" || cost?.belong === "C") &&
                cost.salaType === "基数"
              ) {
                tmp_cost.BC_js_cost += cost.useCost;
                remain_cost.BC_js_cost -= cost.useCost;
              } else if (
                (cost?.belong === "B" || cost?.belong === "C") &&
                cost.salaType === "劳动竞赛"
              ) {
                tmp_cost.BC_ldjs_cost += cost.useCost;
                remain_cost.BC_ldjs_cost -= cost.useCost;
              }
            }
          });
  
          [use_cost, tmp_cost, remain_cost].forEach(item=>{
              item.A_fgz_cost = NumRound(item.A_fgz_cost,2);
              item.A_gz_cost  = NumRound(item.A_gz_cost,2 );
              item.A_ldjs_cost= NumRound(item.A_ldjs_cost,2);
              item.BC_js_cost = NumRound(item.BC_ldjs_cost,2);
              item.BC_ldjs_cost= NumRound(item.BC_ldjs_cost,2);   
          });

        return [use_cost, tmp_cost, remain_cost] as IDeptMoney[];
    
    }else
        return  [] as IDeptMoney[];

  });
 
  

  //2.结余+超额，columns
  //定义数据表列
  const use_columns = [
    { title: "类型", key: "desc", dataIndex: "desc" },
    {
      title: "A工资总额(元)",
      key: "A_gz_cost",
      dataIndex: "A_gz_cost",
      align: "right",
    },
    {
      title: "A非工资总额(元)",
      key: "A_fgz_cost",
      dataIndex: "A_fgz_cost",
      align: "right",
    },
    {
      title: "A劳动竞赛(元)",
      key: "A_ldjs_cost",
      dataIndex: "A_ldjs_cost",
      align: "right",
    },
    {
      title: "BC基数(元)",
      key: "BC_js_cost",
      dataIndex: "BC_js_cost",
      align: "right",
    },
    {
      title: "BC劳动竞赛(元)",
      key: "BC_ldjs_cost",
      dataIndex: "BC_ldjs_cost",
      align: "right",
    },
  ];

  const rowKey = (record) => record.level;
  // debugger 在浏览器断点
  return { use_costs, use_columns, rowKey };
}

export {
  getDeptSalaTableInfo,
  getDeptDetailTabInfo,
  getDeptList,
  getMockData,
  mockData,
  NumRound,
};
