import {createRouter,createWebHashHistory} from 'vue-router'

import mockSala from '../views/mockSala.vue'
import monitorView from '../views/monitorView.vue'
import GaodeMapVue from '../components/GaodeMap.vue'
import HelloWorld from  '../components/HelloWorld.vue';

const routes = [
    
    {path:'/',component:HelloWorld,title:'首页demo' },
    {path:'/sala',component:mockSala,title:'模拟发放' },
    {path:'/monitor',component:monitorView,title:'监控'},
 ]

 const router = createRouter({
    history: createWebHashHistory(),
    routes,
 })


 export { router }